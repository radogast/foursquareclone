//
//  AppDelegate.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/13/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds
import FBSDKCoreKit
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        print(urls[urls.count - 1] as URL)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let mainVC = MainTabBarcontroller()
        window?.rootViewController = mainVC
        GADMobileAds.configure(withApplicationID: "ca-app-pub-6455105743002746~7234955989")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        STPPaymentConfiguration.shared().publishableKey = "pk_test_Qq8xPqU8f6Lcp442pXiorWXJ"
        return true
    }
    func applicationWillTerminate(_ application: UIApplication) {
        let venueDetails = FoursquareManager.sharedManager().arrayOfDetails
        if !venueDetails.isEmpty {
         UserDefaults.standard.set(try? PropertyListEncoder().encode(venueDetails), forKey: "caches")
        }
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
          FBSDKAppEvents.activateApp()
    }
}
