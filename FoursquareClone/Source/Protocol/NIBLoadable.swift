//
//  NIBLoadable.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/13/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//
//-MARK return NIB View compare

import Foundation
import UIKit

protocol NIBLoadable: class {
    static var nib: UINib {
        get
    }
}

extension NIBLoadable {
    static var nib: UINib {
        return UINib(nibName: name, bundle: Bundle.init(for: self))
    }
    static var name: String {
        return String(describing: self)
    }
}

extension NIBLoadable where Self: UIView {
    static func loadFromNib () -> Self {
        guard  let view = nib.instantiate(withOwner: nil, options: nil).first  as? Self else {
            fatalError()
        }
        return view
    }

}
