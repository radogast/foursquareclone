//
//  ListsRouter.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class ListsRouter {
    static let shared = ListsRouter()
    func goToDiscoverFeaturedLists(from source: UIViewController) {
        let viewController = FeaturedListsVC()
        source.navigationController?.pushViewController(viewController, animated: true)
    }
}
