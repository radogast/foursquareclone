//
//  ViewController.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/17/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

class MainTabBarcontroller: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        setupTabBar()
    }
    func setupTabBar() {
        let searchController = UINavigationController(rootViewController: ViewController())
        searchController.tabBarItem.image = UIImage(named: "star-2")
        searchController.tabBarItem.title = "Search"
        let listVC = UINavigationController(rootViewController: ListsViewController())
        listVC.tabBarItem.image = UIImage(named: "star-2")
        listVC.tabBarItem.title = "List"
        let authVC = UINavigationController(rootViewController: AuthVC())
        authVC.tabBarItem.image = UIImage(named: "star-2")
        authVC.tabBarItem.title = "Register"
        viewControllers = [searchController, listVC, authVC]
    }
}
