//
//  StartRouter.swift
//  FoursquareClone
//
//  Created by Radagast on 12/26/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

final class StartRouter {
    static let shared  = StartRouter()
    private init() {}
     func goToSearchTabel(from source: UIViewController) {
        let viewController = SearchTabelVC()
        source.navigationController?.pushViewController(viewController, animated: true)
    }
}
