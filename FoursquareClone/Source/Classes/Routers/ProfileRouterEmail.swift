//
//  ProfileRouterEmail.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit
class ProfileRouterEmail {
    static let shared = ProfileRouterEmail()
    private init() {}
    func goToSingUp(from source: UIViewController) {
        let viewController = SignUpVC()
        source.navigationController?.pushViewController(viewController, animated: true)
    }
    func goToLogin(from source: UIViewController) {
        let viewController = LogInVC()
        source.navigationController?.pushViewController(viewController, animated: true)
    }
    func goToPasswordReset(from source: UIViewController) {
        let viewController = ResetPasswordVC()
        source.navigationController?.pushViewController(viewController, animated: true)
    }

}
