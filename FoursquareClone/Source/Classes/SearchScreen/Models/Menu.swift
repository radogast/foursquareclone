//
//  Menu.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/27/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import Foundation

struct Menu {
    var name: String?
    var imageName: String?
}
