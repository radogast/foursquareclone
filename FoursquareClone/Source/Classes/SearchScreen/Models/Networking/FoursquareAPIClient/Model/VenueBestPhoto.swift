//
//  VenueBestPhoto.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/10/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct VenueBestPhoto: Codable {
    let prefix: String?
    let suffix: String?
    var venuePhoto: String {
        if let pref = prefix, let suff = suffix {
           return String(format: "%@%d%@", pref, 88, suff)
        } else {
            return ""
        }
    }
}
