//
//  Sample.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/18/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct Items: Codable {
    let text: String?
    private enum CodingKeys: String, CodingKey {
        case text
    }
}
