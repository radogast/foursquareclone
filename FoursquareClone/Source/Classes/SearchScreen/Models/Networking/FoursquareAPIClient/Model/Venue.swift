//
//  Venue.swift
//  VenueMap
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

struct Venue: Codable {
    let venueId: String
    let name: String
    let location: Location?

    private enum CodingKeys: String, CodingKey {
        case venueId = "id"
        case name
        case location
    }
}
