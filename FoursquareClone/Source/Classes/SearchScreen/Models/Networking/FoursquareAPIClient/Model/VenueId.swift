//
//  VenueId.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/10/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct VenueID: Codable {
    let venueId: String?
    let name: String
    let price: Price?
    let rating: Double?
    let ratingColor: String?
    let bestPhoto: VenueBestPhoto?
    let location: Location
    let categories: [VenueCategory]?
    let tips: Tips?

    private enum CodingKeys: String, CodingKey {
        case location
        case venueId = "id"
        case name
        case rating
        case bestPhoto
        case price
        case categories
        case tips
        case ratingColor
    }
}
