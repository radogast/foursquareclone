//
//  Checkin.swift
//  FoursquareAPIClient
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation

struct Checkin: Codable {
    let checkinId: String
    let venue: Venue

    private enum CodingKeys: String, CodingKey {
        case checkinId = "id"
        case venue
    }
}
