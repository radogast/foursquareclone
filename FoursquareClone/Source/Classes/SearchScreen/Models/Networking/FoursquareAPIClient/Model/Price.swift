//
//  Price.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/10/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct Price: Codable {
    let message: String?
    let currency: String?

    private enum CodingKeys: String, CodingKey {
        case message
        case currency
    }
}
