//
//  Phrase.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/10/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct Tips: Codable {
    let count: Int?
    let groups: [Groups]?
    private enum CodingKeys: String, CodingKey {
    case count
    case groups
    }
}
