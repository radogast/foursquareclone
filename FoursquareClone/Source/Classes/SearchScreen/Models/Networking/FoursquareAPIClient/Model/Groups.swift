//
//  Groups.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/22/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct Groups: Codable {
    let items: [Items]?
    private enum CodingKeys: String, CodingKey {
        case items
    }
}
