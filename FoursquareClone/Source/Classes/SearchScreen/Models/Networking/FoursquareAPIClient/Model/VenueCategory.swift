//
//  VenueCategory.swift
//  FoursquareAPIClient
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation

struct VenueCategory: Codable {
    let categoryId: String
    let name: String?
    let icon: VenueCategoryIcon

    private enum CodingKeys: String, CodingKey {
        case categoryId = "id"
        case name
        case icon
    }
}
