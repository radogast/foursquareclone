//
//  Location.swift
//  FoursquareAPIClient
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//
import Foundation

struct Location: Codable {
    let address: String?
    let latitude: Double
    let longitude: Double
    let city: String?
    let countedDistance: String?
    let distance: Int?
    private enum CodingKeys: String, CodingKey {
        case countedDistance
        case city
        case address
        case latitude = "lat"
        case longitude = "lng"
        case distance
    }
}
