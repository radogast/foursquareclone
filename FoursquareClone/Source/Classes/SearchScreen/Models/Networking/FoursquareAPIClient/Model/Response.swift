//
//  Response.swift
//  FoursquareAPIClient
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation

struct Response <Response: Codable> : Codable {
    let response: Response
}
