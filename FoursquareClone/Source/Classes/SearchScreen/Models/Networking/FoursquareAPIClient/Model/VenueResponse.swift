//
//  VenueResponse.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/10/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation

struct VenueResponse: Codable {
    let venue: VenueID
}
