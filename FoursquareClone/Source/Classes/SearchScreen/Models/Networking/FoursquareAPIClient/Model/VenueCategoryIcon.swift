//
//  VenueCategoryIcon.swift
//  FoursquareAPIClient
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation

struct VenueCategoryIcon: Codable {
    let prefix: String
    let suffix: String

    var categoryIconUrl: String {
        return String(format: "%@%d%@", prefix, 88, suffix)
    }
}
