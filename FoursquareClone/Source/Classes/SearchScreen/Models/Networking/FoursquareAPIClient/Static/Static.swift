//
//  Static.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/24/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct Static {
    static let instance = FoursquareManager()
}
