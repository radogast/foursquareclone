//
//  FoursquareManager.swift
//  VenueMap
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class FoursquareManager: NSObject {
    var clientId: String = "IFHUZQ3VF0XQMITSNGM0OOEWL0PYDLJEMO3WDJTBSL10DO1J"
    var clientSecret: String = "JHKEOSTCDBGWRKYC0K0C0YSNZGERIM45A14HK3DQPWCCK2KX"
    var accessToken: String!
    var venues: [Venue] = []
    var venueId: VenueID?
    var arrayOfDetails: [VenueID] = []
    class func sharedManager() -> FoursquareManager {
        return Static.instance
    }
    func searchVenuesWithCoordinate(_ coordinate: CLLocationCoordinate2D, completion: ((Error?) -> Void)?) {
        let client = FoursquareAPIClient(clientId: clientId, clientSecret: clientSecret)
        let parameter: [String: String] = [
            "ll": "\(coordinate.latitude),\(coordinate.longitude)"
        ]
        client.request(path: "venues/search", parameter: parameter) { [weak self] result in
            switch result {
            case let .success(data):
                let decoder: JSONDecoder = JSONDecoder()
                do {
                    let response = try decoder.decode(Response<SearchResponse>.self, from: data)
                    self?.venues = response.response.venues
                    completion?(nil)
                } catch {
                    completion?(error)
                }
            case let .failure(error):
                completion?(error)
            }
        }
    }
    func getDetailOfVenue(_ venueID: String, completion: ((Error?) -> Void)?) {
        let client = FoursquareAPIClient(clientId: clientId, clientSecret: clientSecret)
        let parameter: [String: String] = ["": ""]
        let path: String = "venues/"+venueID
        client.request(path: path, parameter: parameter) { [weak self] result in
            switch result {
            case let .success(data):
                let decoder: JSONDecoder = JSONDecoder()
                do {
                    let response = try decoder.decode(Response<VenueResponse>.self, from: data)
                    self?.venueId = response.response.venue
                    completion?(nil)
                } catch {
                    completion?(error)
                }
            case let .failure(error):
                completion?(error)
            }
        }
    }
}
