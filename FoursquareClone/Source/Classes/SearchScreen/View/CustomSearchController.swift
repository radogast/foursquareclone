//
//  CustomSearchController.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/28/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

class CustomSearchController: UISearchController {
    lazy var serchbar: CustomSearchBar = {
        [ unowned self] in
            let customSearchBar = CustomSearchBar(frame: CGRect.zero)
                return customSearchBar
            }()
}
