//
//  CustomSearchBar.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/28/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

class CustomSearchBar: UISearchBar {
    override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
        super.setShowsCancelButton(false, animated: false)
    }
}
