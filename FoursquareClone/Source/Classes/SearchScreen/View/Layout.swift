//
//  Layout.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/21/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class AnnotationFlowLayout: UICollectionViewFlowLayout {
    override open func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
// MARK: Custom paging 
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint,
                                      withScrollingVelocity velocity: CGPoint) -> CGPoint {

        guard let collectionView = collectionView else {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
        }
        let targetRect = CGRect(origin: proposedContentOffset, size: collectionView.bounds.size)
        let visibleCellsLayoutAttributes = layoutAttributesForElements(in: targetRect)
        let candidateOffsets: [CGFloat]? = visibleCellsLayoutAttributes?.map({ cellLayoutAttributes in

            return cellLayoutAttributes.frame.origin.x - collectionView.contentInset.left - collectionView.safeAreaInsets.left - sectionInset.left
        })

        let bestCandidateOffset: CGFloat
        if velocity.x > 0 {
            let candidateOffsetsToRight = candidateOffsets?.toRight(ofProposedOffset: proposedContentOffset.x)
            let nearestCandidateOffsetToRight = candidateOffsetsToRight?.nearest(toProposedOffset: proposedContentOffset.x)
            bestCandidateOffset = nearestCandidateOffsetToRight ?? candidateOffsets?.last ?? proposedContentOffset.x
        } else if velocity.x < 0 {
            let candidateOffsetsToLeft = candidateOffsets?.toLeft(ofProposedOffset: proposedContentOffset.x)
            let nearestCandidateOffsetToLeft = candidateOffsetsToLeft?.nearest(toProposedOffset: proposedContentOffset.x)
            bestCandidateOffset = nearestCandidateOffsetToLeft ?? candidateOffsets?.first ?? proposedContentOffset.x
        } else {
            let nearestCandidateOffset = candidateOffsets?.nearest(toProposedOffset: proposedContentOffset.x)
            bestCandidateOffset = nearestCandidateOffset ??  proposedContentOffset.x
        }
        return CGPoint(x: bestCandidateOffset, y: proposedContentOffset.y)
    }
}
fileprivate extension Sequence where Iterator.Element == CGFloat {
    func toLeft(ofProposedOffset proposedOffset: CGFloat) -> [CGFloat] {

        return filter { candidateOffset in
            return candidateOffset < proposedOffset
        }
    }
    func toRight(ofProposedOffset proposedOffset: CGFloat) -> [CGFloat] {
        return filter { candidateOffset in
            return candidateOffset > proposedOffset
        }
    }
    func nearest(toProposedOffset proposedOffset: CGFloat) -> CGFloat? {
        guard let firstCandidateOffset = first(where: { _ in true }) else {
            return nil
        }
        return reduce(firstCandidateOffset) { (bestCandidateOffset: CGFloat, candidateOffset: CGFloat) -> CGFloat in

            let candidateOffsetDistanceFromProposed = abs(candidateOffset - proposedOffset)
            let bestCandidateOffsetDistancFromProposed = abs(bestCandidateOffset - proposedOffset)

            if candidateOffsetDistanceFromProposed < bestCandidateOffsetDistancFromProposed {
                return candidateOffset
            }
            return bestCandidateOffset
        }
    }
}
