//
//  LookAround.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/24/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

class LookAround: UITableViewCell {

    @IBOutlet weak var leftImage: UIImageView?
    @IBOutlet weak var rightImage: UIImageView?

    @IBOutlet weak var leftVenueName: UILabel?
    @IBOutlet weak var rightVenueName: UILabel?
}
