//
//  SearchFiltersCell.swift
//  FoursquareClone
//
//  Created by Radagast on 12/27/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

class SearchFiltersCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var view: UIImageView!

    var menu: Menu? {
        didSet {
        label.text = menu?.name
        if let image = menu?.imageName {
            view.image = UIImage(named: image)
            }
        }
    }
}
extension SearchFiltersCell: NIBLoadable {}
