//
//  AnnotationViewCell.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/8/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class AnnotationViewCell: UICollectionViewCell {
    @IBOutlet weak var annotationImage: UIImageView!
    @IBOutlet weak var annotationTitle: UILabel!
    @IBOutlet weak var annotationTypeOfFoodLabel: UILabel!
    @IBOutlet weak var annotationDistance: UILabel!
    @IBOutlet weak var annotationScore: UILabel!
    private let radius: CGFloat = 5.0
    override func awakeFromNib() {
        super.awakeFromNib()
        annotationImage.layer.cornerRadius = radius
        annotationScore.layer.cornerRadius = radius
        annotationScore.layer.masksToBounds = true
        annotationImage.layer.masksToBounds = true
    }
    var setData: VenueID? {
        didSet {
            var photoURL: URL?
            annotationTitle.text = setData?.name
            annotationTypeOfFoodLabel.text = setData?.categories?.first?.name
            if let price = setData?.price?.currency, let categoryName = setData?.categories?.first?.name {
                let categoryWithSpace = categoryName + " "
                let priceLevel = "$$$$"
                var mutabelString = NSMutableAttributedString()
                mutabelString = NSMutableAttributedString(string: categoryWithSpace + priceLevel,
                                                          attributes: [NSAttributedString.Key.font: UIFont(name: "Helvetica",
                                                                                                           size: 14.0)!])
                mutabelString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray,
                                           range: NSRange(location: categoryWithSpace.count + price.count,
                                                          length: priceLevel.count - price.count))
                annotationTypeOfFoodLabel.attributedText = mutabelString
            }
            annotationScore.text = ""
            annotationScore.backgroundColor = .clear
            if let venueRating = setData?.rating {
                let color = setData?.ratingColor!
                annotationScore.backgroundColor = UIColor(hexFromString: color!)
                annotationScore.text = String(format: "%.1f", venueRating )
            }
            if let categories = setData?.categories,
                let details = setData?.bestPhoto?.venuePhoto {
                if !details.isEmpty {
                    photoURL = URL(string: details)
                    annotationImage.af_setImage(withURL: photoURL!)
                } else if !categories.isEmpty {
                    photoURL = URL(string: categories[0].icon.categoryIconUrl)
                    annotationImage.af_setImage(withURL: photoURL!)
                }
            }
        }
    }
}

extension AnnotationViewCell: NIBLoadable {}
