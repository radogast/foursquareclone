//
//  VenueTableCell.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/10/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class VenueTableCell: UITableViewCell {

    @IBOutlet weak var venueSchot: UIImageView!
    @IBOutlet weak var vanueTitle: UILabel!
    @IBOutlet weak var venueDistance: UILabel!
    @IBOutlet weak var venueTip: UILabel!
    @IBOutlet weak var venueScore: UILabel!
    @IBOutlet weak var venuePrice: UILabel!
    private let radius: CGFloat = 5.0

    override func awakeFromNib() {
        super.awakeFromNib()
        venueSchot.layer.cornerRadius = radius
        venueScore.layer.cornerRadius = radius
        venueScore.layer.masksToBounds = true
        venueSchot.layer.masksToBounds = true

    }

    var setData: VenueID? {
        didSet {
            var photoURL: URL?
            vanueTitle.text = setData?.name

            venueSchot.layer.cornerRadius = radius
            venueScore.layer.cornerRadius = radius

            venuePrice.text = setData?.categories?.first?.name
            if let price = setData?.price?.currency, let categoryName = setData?.categories?.first?.name {
                let categoryWithSpace = categoryName + " "
                let priceLevel = "$$$$"
                var mutabelString = NSMutableAttributedString()
                mutabelString = NSMutableAttributedString(string: categoryWithSpace + priceLevel,
                                                          attributes: [NSAttributedString.Key.font: UIFont(name: "Helvetica",
                                                                                                          size: 17.0)!])
                mutabelString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray,
                                           range: NSRange(location: categoryWithSpace.count + price.count,
                                                          length: priceLevel.count - price.count))
                venuePrice.attributedText = mutabelString
            }
            venueScore.text = ""
            venueScore.backgroundColor = .clear
            if let venueRating = setData?.rating {
                let color = setData?.ratingColor!
                venueScore.backgroundColor = UIColor(hexFromString: color!)
                venueScore.text = String(format: "%.1f", venueRating )
            }
            if let tip = setData?.tips!.groups?.first?.items?.first?.text {
                venueTip?.text = "\u{22}" + tip + "\u{22}"
            }
            if let categories = setData?.categories,
                let details = setData?.bestPhoto?.venuePhoto {
                if !details.isEmpty {
                    photoURL = URL(string: details)
                    venueSchot.af_setImage(withURL: photoURL!)
                } else if !categories.isEmpty {
                    photoURL = URL(string: categories[0].icon.categoryIconUrl)
                    venueSchot.af_setImage(withURL: photoURL!)
                }
            }
        }
    }
}
extension VenueTableCell: NIBLoadable {}
