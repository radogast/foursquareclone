//
//  ViewController.swift
//  FoursquareClone
//
//  Created by Radagast on 12/25/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ViewController: UIViewController {
// MARK: - array of menu buttons
    private var itemsMenuArray: [Menu] = {
        var lunchCell = Menu()
        lunchCell.name = "Lunch"
        lunchCell.imageName = "lunchImage"

        var brekfastCell = Menu()
        brekfastCell.name = "Brekfast"
        brekfastCell.imageName = "breakfastImage"

        var dinnerCell = Menu()
        dinnerCell.name = "Dinner"
        dinnerCell.imageName = "dinnerImage"

        var nightlifeCell = Menu()
        nightlifeCell.name = "Nightlife"
        nightlifeCell.imageName = "nightlifeImage"

        var thingToDoCell = Menu()
        thingToDoCell.name = "Thing to do"
        thingToDoCell.imageName = "thingToDoImage"

        var coffeeAndTeaCell = Menu()
        coffeeAndTeaCell.name = "Coffee & Tea"
        coffeeAndTeaCell.imageName = "coffee&TeaImage"

        return [lunchCell, brekfastCell, nightlifeCell, dinnerCell, thingToDoCell, coffeeAndTeaCell]
    }()
    @IBOutlet weak var  collectionView: UICollectionView!
    @IBOutlet weak var  searchButton: UIButton!
    var interstitial: GADInterstitial!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setCollectionView()
        setButtonView()
       interstitial = createAndLoadInterstitial()
    }

    private func setButtonView() {
        searchButton.layer.shadowColor = UIColor.black.cgColor
        searchButton.layer.shadowOffset = CGSize.zero
        searchButton.layer.shadowOpacity = 0.5
        searchButton.layer.shadowRadius = 10
        searchButton.layer.masksToBounds = false
        searchButton.addTarget(self, action: #selector(sendingButtonsClicked), for: .touchUpInside)
    }
    private func setCollectionView() {
        collectionView.register(SearchFiltersCell.nib, forCellWithReuseIdentifier: SearchFiltersCell.name)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
// MARK: - Hide navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
// MARK: - Button selector
    @objc private func sendingButtonsClicked() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
        StartRouter.shared.goToSearchTabel(from: self)
    }

}
// MARK: - collection DataSource
extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if let venueCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchFiltersCell",
                                                              for: indexPath) as? SearchFiltersCell {
            venueCell.menu = itemsMenuArray[indexPath.row]
            cell = venueCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        StartRouter.shared.goToSearchTabel(from: self)
    }
}
// MARK: - collection FlowLayout
extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 105, height: 110)
    }
}
extension ViewController: GADInterstitialDelegate {
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    @objc func showAds() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
}
