//
//  SearchTabelVC.swift
//  FoursquareClone
//
//  Created by Radagast on 12/26/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.

import UIKit
import MapKit
import AlamofireImage
import GoogleMobileAds

class SearchTabelVC: UIViewController {
    // MARK: - Properties
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchField: UITextField!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var collectionViewFilter: UICollectionView!
    @IBOutlet private weak var collectionViewMapAnnotaionPicker: UICollectionView!
    // larger ad interval to avoid mutliple ads being on screen at the same time.
    let adInterval = UIDevice.current.userInterfaceIdiom == .pad ? 16 : 8
    var adsToLoad = [GADBannerView]()
    var loadStateForAds = [GADBannerView: Bool]()
    let adUnitID = "ca-app-pub-3940256099942544/2934735716"
    let adViewHeight = CGFloat(100)
    private var currentPage: Int = 0
    private var arrayOfDetails: [Any] = [] {
        didSet {
            if arrayOfDetails.count == 30 {
                addBannerAds()
                preloadNextAd()
            }
        }
    }
    private var mapState: Bool = true
    private var isLocationInitialized = false
    // MARK: - UI Properties
    private let mapButton = UIButton(type: .custom)
    private let searchController: UISearchController = ({
        let controller = CustomSearchController(searchResultsController: nil)
        controller.searchBar.placeholder = NSLocalizedString("Near me",
                                                             comment: "")
        controller.hidesNavigationBarDuringPresentation = false
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.searchBarStyle = .minimal
        controller.searchBar.sizeToFit()
        return controller
    })()
    private let secondSearchController: UISearchController = ({
        let controller = CustomSearchController(searchResultsController: nil)
        controller.searchBar.placeholder = NSLocalizedString("What are you loking for",
                                                             comment: "")
        controller.hidesNavigationBarDuringPresentation = false
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.searchBarStyle = .minimal
        controller.searchBar.sizeToFit()
        return controller
    })()
    private let refreshControl = UIRefreshControl()
    private var locationManager = CLLocationManager()
    private var userLocation: CLLocation?
    private var indicator = UIActivityIndicatorView()
    // MARK: - View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        setRefreshControl()
        setSpinner()
        collectionViewFilter.isHidden = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .darkGray
        view.backgroundColor = .darkGray
        setFlowAtCollectionView()
        self.navigationController?.navigationBar.setValue(true,
                                                          forKey: "hidesShadow")
        definesPresentationContext = true
        setMapButton()
        prepareForMap()
        setCollectionViews()
        setSearchControllers()
        setTabelView()
        setMap()
    }
    private func prepareForMap() {
        mapView.isHidden = true
        mapView.showsUserLocation = true
        mapView.delegate = self
    }
    private func setSearchControllers() {
        navigationItem.titleView = searchController.searchBar
        navigationItem.searchController?.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController?.hidesBottomBarWhenPushed = false
    }
    private func setRefreshControl() {
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        refreshControl.tintColor = .blue
    }
    private func setTabelView() {
        tableView.isHidden = true
        tableView.refreshControl = refreshControl
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.contentInsetAdjustmentBehavior = .never
        tableView.register(VenueTableCell.nib,
                           forCellReuseIdentifier: VenueTableCell.name)
        tableView.register(UINib(nibName: "BannerAdCell", bundle: nil),
                           forCellReuseIdentifier: "BannerAdCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
    }
    private func setCollectionViews() {
        collectionViewMapAnnotaionPicker.isHidden = true
        collectionViewMapAnnotaionPicker.register(AnnotationViewCell.nib,
                                                  forCellWithReuseIdentifier: AnnotationViewCell.name)
        collectionViewMapAnnotaionPicker.delegate = self
        collectionViewMapAnnotaionPicker.dataSource = self
        collectionViewMapAnnotaionPicker.decelerationRate = .fast
    }
    private func setMapButton() {
        mapButton.setTitle("map", for: .normal)
        mapButton.addTarget(self, action: #selector(showMap), for: .touchUpInside)
        let mapButtonItem = UIBarButtonItem(customView: mapButton)
        navigationItem.rightBarButtonItem = mapButtonItem
    }
    private func setSpinner() {
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        indicator.center = self.view.center
        self.view.addSubview(indicator)
        indicator.startAnimating()
    }
    // MARK: - Private  methods
    @objc private func showMap() {
        if mapState {
            mapState = false
        } else {
            mapState = true
        }
        hideOrShowsMaps()
    }
    @objc private func reloadData() {
        if isLocationInitialized {
            isLocationInitialized = false
        }
    }
    private func setFlowAtCollectionView() {
        if let flowLayout = self.collectionViewMapAnnotaionPicker.collectionViewLayout as? UICollectionViewFlowLayout {
            let insets: CGFloat = 20.0
            let cellWidth: CGFloat = collectionViewMapAnnotaionPicker.bounds.width - insets
            flowLayout.itemSize.width = cellWidth
            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
            let insetX = flowLayout.minimumInteritemSpacing / 2.0
            flowLayout.sectionInset = UIEdgeInsets(top: 0,
                                                   left: insetX,
                                                   bottom: 0,
                                                   right: insetX)
        }
    }
    private func setMap() {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager = locationManager
        let venues = arrayOfDetails
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 10,
                                                longitudinalMeters: 10)
            mapView.setRegion(viewRegion, animated: false)
        }
        for (index, locations) in venues.enumerated() {
            if let location = locations as? VenueID {
                let annotation = MKPointAnnotation()
                annotation.title = String(index + 1)
                annotation.subtitle = location.name
                annotation.coordinate = CLLocationCoordinate2D(latitude: location.location.latitude,
                                                               longitude: location.location.longitude)
                self.mapView.addAnnotation(annotation)
            }
        }
        self.locationManager.startUpdatingLocation()
    }
    private func hideOrShowsMaps() {
        if mapState {
            collectionViewMapAnnotaionPicker.isHidden = true
            mapView.isHidden = true
            tableView.isHidden = false
            self.tableView.reloadData()
            self.collectionViewMapAnnotaionPicker.reloadData()
        } else {
            collectionViewMapAnnotaionPicker.isHidden = false
            mapView.isHidden = false
            tableView.isHidden = true
            self.tableView.reloadData()
            self.collectionViewMapAnnotaionPicker.reloadData()
        }
    }
    private func updateVenuesRequest(_ coordinate: CLLocationCoordinate2D) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        FoursquareManager.sharedManager().searchVenuesWithCoordinate(coordinate, completion: { [weak self] (_) in
            let checkArray = self?.arrayOfDetails.isEmpty
            if !checkArray! {
                for (index, venue) in FoursquareManager.sharedManager().venues.enumerated() {
                    if let details = self?.arrayOfDetails[index] as? VenueID {
                        if venue.venueId != details.venueId {
                            FoursquareManager.sharedManager().getDetailOfVenue(venue.venueId, completion: { [weak self] (_) in
                                if let venueId = FoursquareManager.sharedManager().venueId {
                                    self?.arrayOfDetails.remove(at: index)
                                    self?.arrayOfDetails.insert(venueId, at: index)
                                    self?.mapView.reloadInputViews()
                                    self?.tableView.reloadData()
                                    self?.collectionViewMapAnnotaionPicker.reloadData()
                                    self?.setMap()
                                    self?.refreshControl.endRefreshing()
                                }
                            })
                        }
                    }
                }
            } else {
                for venue in FoursquareManager.sharedManager().venues {
                    FoursquareManager.sharedManager().getDetailOfVenue(venue.venueId, completion: { [weak self] (_) in
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self?.tableView.isHidden = false
                        if let venueId = FoursquareManager.sharedManager().venueId {
                            self?.arrayOfDetails.append(venueId)
                        }
                        self?.mapView.reloadInputViews()
                        self?.tableView.reloadData()
                        self?.collectionViewMapAnnotaionPicker.reloadData()
                        self?.setMap()
                        self?.indicator.stopAnimating()
                    })
                }
            }
        })
    }
    private  func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    private func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
}
// TabelView DataSource
extension SearchTabelVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let bannerView = arrayOfDetails[indexPath.row] as? GADBannerView {
            let reusableAdCell = tableView.dequeueReusableCell(withIdentifier: "BannerAdCell",
                                                               for: indexPath)
            for subview in reusableAdCell.contentView.subviews {
                subview.removeFromSuperview()
            }
            reusableAdCell.contentView.addSubview(bannerView)
// Center GADBannerView in the table cell's content view.
            bannerView.center = reusableAdCell.contentView.center
            return reusableAdCell
        } else {
            if let venueCell = tableView.dequeueReusableCell(withIdentifier: "VenueTableCell",
                                                             for: indexPath) as? VenueTableCell {
                if let venue = arrayOfDetails[(indexPath).row] as? VenueID {
                    let venueDistance = FoursquareManager.sharedManager().venues
                    for id in venueDistance where venue.venueId == id.venueId {
                        if let cityName = venue.location.city, let distance = id.location?.distance {
                            venueCell.venueDistance.text = String(distance) + " " + "m" + " " + cityName
                        }
                    }
                    venueCell.setData = venue
                }
                cell = venueCell
            }
            return cell
        }
    }
}
extension SearchTabelVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrayOfDetails[indexPath.row] is GADBannerView {
            return  adViewHeight
        } else {
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let venueCell = tableView.dequeueReusableCell(withIdentifier: "VenueTableCell",
                                                         for: indexPath) as? VenueTableCell {
            if let image  = venueCell.venueSchot.image {
          let iga = InstagramActivity(with: .view(view,
                                                  .init(origin: view.center,
                                                        size: .init(width: 100, height: 100))),
                                      in: self)
        let activityViewController = UIActivityViewController(activityItems: [image], applicationActivities: [iga])
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
}
// CollectionView DataSource
extension SearchTabelVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrayOfDetails.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if let venueCell = collectionViewMapAnnotaionPicker.dequeueReusableCell(withReuseIdentifier: "AnnotationViewCell",
                                                                                for: indexPath) as? AnnotationViewCell {
            if  let venue = arrayOfDetails[indexPath.section] as? VenueID {
                if let cityName = venue.location.city {
                    venueCell.annotationDistance.text = cityName
                }
                let venueDistance = FoursquareManager.sharedManager().venues
                for id in venueDistance where venue.venueId == id.venueId {
                    if let cityName = venue.location.city, let distance = id.location?.distance {
                        venueCell.annotationDistance.text = String(distance) + " " + "m" + " " + cityName
                    }
                }
                venueCell.setData = venue
            }
            cell = venueCell
        }
        return cell
    }
}

extension SearchTabelVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
// MARK: Update location at simulator and drag refresh control to start downloading
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        guard let newLocation = locations.last,
            CLLocationCoordinate2DIsValid(newLocation.coordinate) else {
                return
        }
        self.userLocation = newLocation
        if isLocationInitialized == false {
            updateVenuesRequest(newLocation.coordinate)
            isLocationInitialized = true
        }
    }
}
//Scroll to collectionView content equally annotation title
extension SearchTabelVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let indexPath = IndexPath(item: 0, section: (((view.annotation?.title as? NSString)?.integerValue)! ) - 1)
        self.collectionViewMapAnnotaionPicker.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}
//Scroll to center of section
extension SearchTabelVC: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == collectionViewMapAnnotaionPicker {
            guard let layout = collectionViewMapAnnotaionPicker.collectionViewLayout as? UICollectionViewFlowLayout else { return }
            let minimum = layout.minimumLineSpacing
            let cellWidthIncludingSpacing = layout.itemSize.width
            var offset = targetContentOffset.pointee
            let index = (offset.x + layout.sectionInset.left) / cellWidthIncludingSpacing
            let roundedIndex = index
            currentPage = Int(roundedIndex.rounded(.towardZero))
            let xOffset = index * cellWidthIncludingSpacing - layout.sectionInset.left
            offset = CGPoint(x: (xOffset - minimum) + 1, y: 0)
            targetContentOffset.pointee = offset
        }
    }
}
//Zoom map equally to collection view cell index
extension SearchTabelVC: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionViewMapAnnotaionPicker {
            let indexPath = IndexPath(item: 0, section: scrollView.currentPage)
            if let venue = arrayOfDetails[indexPath.section] as? VenueID {
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: venue.location.latitude,
                                                               longitude: venue.location.longitude)
                let viewRegion = MKCoordinateRegion(center: annotation.coordinate,
                                                    latitudinalMeters: 10,
                                                    longitudinalMeters: 10)
                mapView.setRegion(viewRegion, animated: true)
            }
        }
    }
}
extension SearchTabelVC: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ adView: GADBannerView) {
        loadStateForAds[adView] = true
        preloadNextAd()
    }
    func adView(_ adView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("Failed to receive ad: \(error.localizedDescription)")
        preloadNextAd()
    }
    func addBannerAds() {
        var index = adInterval
        tableView.layoutIfNeeded()
        while index < arrayOfDetails.count {
            let adSize = GADAdSizeFromCGSize(
                CGSize(width: tableView.contentSize.width,
                       height: adViewHeight))
            let adView = GADBannerView(adSize: adSize)
            adView.adUnitID = adUnitID
            adView.rootViewController = self
            adView.delegate = self
            arrayOfDetails.insert(adView, at: index)
            adsToLoad.append(adView)
            loadStateForAds[adView] = false
            index += adInterval
        }
    }
    func preloadNextAd() {
        if !adsToLoad.isEmpty {
            let ad = adsToLoad.removeFirst()
            let adRequest = GADRequest()
            adRequest.testDevices = [ kGADSimulatorID ]
            ad.load(adRequest)
        }
    }
}
