//
//  CarouselNewsCell.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/14/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit

class CarouselViewCell: UICollectionViewCell {
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsShortcuts: UILabel!
    @IBOutlet weak var newsImage: UIImageView!

    override func awakeFromNib() {

    }
    var list: FeaturedList? {
        didSet {
            newsShortcuts.text = list?.description
            newsTitle.text = list?.title
            if let image = list?.mainImage {
                newsImage.image = UIImage(named: image)
                userPhoto.image = UIImage(named: image)
            }

        }
    }

}
extension CarouselViewCell: NIBLoadable {}
