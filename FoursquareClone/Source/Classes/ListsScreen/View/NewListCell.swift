//
//  NewListCell.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/4/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class NewListCell: UICollectionViewCell {
    @IBOutlet weak var cornerButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerButton.layer.cornerRadius = (cornerButton.frame.width / 2 + cornerButton.frame.height / 2) / 2
    }
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame = layoutAttributes.frame
        frame.size.height = ceil(size.height)
        layoutAttributes.frame = frame
        return layoutAttributes
    }
}
extension NewListCell: NIBLoadable {}
