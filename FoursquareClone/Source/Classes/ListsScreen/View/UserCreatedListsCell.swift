//
//  UserCreatedListsCell.swift
//  FoursquareClone
//
//  Created by Radagast on 1/7/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class UserCreatedListsCell: UICollectionViewCell {
    @IBOutlet weak var listImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
extension UserCreatedListsCell: NIBLoadable {}
