//
//  DiagonalCustomView.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/3/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

// MARK: - Draw a diagonal cut

class DiagonalCustomView: UIView {

    let fillColor: UIColor = UIColor.white

    private var points: [CGPoint] = [
        .zero,
        CGPoint(x: 0, y: 1),
        CGPoint(x: 1, y: 1),
        CGPoint(x: 4, y: 1)
        ] { didSet { setNeedsLayout() } }

    private lazy var shapeLayer: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        self.layer.insertSublayer(shapeLayer, at: 0)
        return shapeLayer
    }()

    override func layoutSubviews() {
        shapeLayer.fillColor = fillColor.cgColor

        guard points.count > 2 else {
            shapeLayer.path = nil
            return
        }

        let path = UIBezierPath()

        path.move(to: convert(relativePoint: points[0]))
        for point in points.dropFirst() {
            path.addLine(to: convert(relativePoint: point))
        }
        path.close()
        shapeLayer.path = path.cgPath
    }

    private func convert(relativePoint point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x * bounds.width + bounds.origin.x, y: point.y * bounds.height + bounds.origin.y)
    }

}
