//
//  UsersPlacesCell.swift
//  FoursquareClone
//
//  Created by Radagast on 1/1/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class UsersPlacesCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet  var placeImages: [UIImageView]?
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var fourthImage: UIImageView!
    @IBOutlet weak var userPhoto: UIImageView!

    var userPlaces: UserPlaces? {
        didSet {
            titleLabel.text = userPlaces?.titleLabel
            descriptionLabel.text = userPlaces?.descriptionLabel
            if let image = userPlaces?.userFirstImage {
                firstImage.image = UIImage(named: image)
                secondImage.image = UIImage(named: image)
                thirdImage.image = UIImage(named: image)
                fourthImage.image = UIImage(named: image)
                userPhoto.image = UIImage(named: image)
                }
            }
        }
}
extension UsersPlacesCell: NIBLoadable {}
