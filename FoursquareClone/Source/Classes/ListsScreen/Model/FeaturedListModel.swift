//
//  FeaturedListModel.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation

struct FeaturedList {
    var userImage: String?
    var title: String?
    var description: String?
    var mainImage: String?
}
