//
//  UserPlaces.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/24/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
struct UserPlaces {
    var userPhoto: String?
    var userFirstImage: String?
    var userSecondImage: String?
    var userThirdImage: String?
    var userForthyImage: String?
    var titleLabel: String?
    var descriptionLabel: String?
}
