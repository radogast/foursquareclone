//
//  ListsViewController.swift
//  FoursquareClone
//
//  Created by Radagast on 1/1/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class ListsViewController: UIViewController {
    @IBOutlet weak var createdListsCounter: UILabel!
    @IBOutlet weak var userCreatedListsCollectionView: UICollectionView!
    @IBOutlet weak var favoritePlacesCollectionView: UICollectionView!
    @IBOutlet weak var featuredListsCollectionView: UICollectionView!
    @IBOutlet weak var discoverButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    private var currentPage: Int = 0
    private var itemsUserCreatedLists: [FeaturedList] = {
        var firstCell = FeaturedList()
        firstCell.description = " l u n c h"
        firstCell.title = "Lunch"
        firstCell.mainImage = "lunchImage"
        firstCell.userImage = "lunchImage"
        return [firstCell, firstCell, firstCell, firstCell, firstCell, firstCell]
    }()
    private var itemsUserPlacesList: [UserPlaces] = {
        var firstCell = UserPlaces()
        firstCell.descriptionLabel = "421 places count"
        firstCell.userFirstImage = "lunchImage"
        firstCell.userPhoto = "lunchImage"
        firstCell.titleLabel = "My liked place"
        var secondCell = UserPlaces()
        secondCell.descriptionLabel = "421 places count"
        secondCell.userFirstImage = "lunchImage"
        secondCell.userPhoto = "lunchImage"
        secondCell.titleLabel = "My saved place"
        return[firstCell, secondCell]
    }()
    private var itemsFeaturedList: [FeaturedList] = {
        var firstCell = FeaturedList()
        firstCell.description = " l u n c h"
        firstCell.title = "Lunch"
        firstCell.mainImage = "lunchImage"
        return [firstCell, firstCell, firstCell, firstCell, firstCell, firstCell]
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = "Lists"
        navigationController?.navigationBar.barTintColor = UIColor.blue
        addTargets()
        setUpCollectionView()
        createdListsCounter.text = "List you created (\(itemsUserCreatedLists.count))"
        setFlowAtCollectionView()
    }
    func setFlowAtCollectionView() {
        if let flowLayout = userCreatedListsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout,
            let collectionView = userCreatedListsCollectionView {
            let width = collectionView.frame.width - 20
            flowLayout.estimatedItemSize = CGSize(width: width, height: 200)
        }
    }
    private func addTargets( ) {
        discoverButton.addTarget(self, action: #selector(verticalFeaturedLists), for: .touchUpInside)
    }
    private func setUpCollectionView() {
        let arrayOfCollectionView: Array = [userCreatedListsCollectionView,
                                            favoritePlacesCollectionView,
                                            featuredListsCollectionView]
        featuredListsCollectionView.decelerationRate = .fast
        featuredListsCollectionView.register(CarouselViewCell.nib,
                                             forCellWithReuseIdentifier: CarouselViewCell.name)
        favoritePlacesCollectionView.register(UsersPlacesCell.nib,
                                              forCellWithReuseIdentifier: UsersPlacesCell.name)
        userCreatedListsCollectionView.register(NewListCell.nib,
                                                forCellWithReuseIdentifier: NewListCell.name)
        userCreatedListsCollectionView.register(UserCreatedListsCell.nib,
                                                forCellWithReuseIdentifier: UserCreatedListsCell.name)
        userCreatedListsCollectionView.sizeToFit()
        arrayOfCollectionView.forEach { (collection: UICollectionView?) in
            collection?.delegate = self
            collection?.dataSource =  self
        }
    }
    @objc private func verticalFeaturedLists() {
        ListsRouter.shared.goToDiscoverFeaturedLists(from: self)
    }
}
// DataSource
extension ListsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        switch collectionView {
        case featuredListsCollectionView:
            pageControl.currentPage = indexPath.section
        default: break
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        switch collectionView {
        case featuredListsCollectionView:
            return itemsFeaturedList.count
        case userCreatedListsCollectionView:
            return itemsUserCreatedLists.count
        default:
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case featuredListsCollectionView:
            return 1
        default:
            return 2
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case favoritePlacesCollectionView:
            var cell = UICollectionViewCell()
            if let favoriteCell = favoritePlacesCollectionView.dequeueReusableCell(withReuseIdentifier: "UsersPlacesCell",
                                                                                   for: indexPath) as? UsersPlacesCell {
                favoriteCell.userPlaces = itemsUserPlacesList[indexPath.row]
                cell = favoriteCell
            }
            return cell
        case userCreatedListsCollectionView:
            if indexPath.row == 0 && indexPath.section == 0 {
                var cell = UICollectionViewCell()
                if let userCreatedCell = userCreatedListsCollectionView.dequeueReusableCell(withReuseIdentifier: "NewListCell",
                                                                                            for: indexPath) as? NewListCell {
                    cell = userCreatedCell
                }
                return cell
            }
            var cell = UICollectionViewCell()
            if let featuredListCell = featuredListsCollectionView.dequeueReusableCell(withReuseIdentifier: "CarouselViewCell",
                                                                                      for: indexPath) as? CarouselViewCell {
                 featuredListCell.list = itemsUserCreatedLists[indexPath.row]
                cell = featuredListCell
            }
            return cell
        default:
            var cell = UICollectionViewCell()
            if let featuredListCell = featuredListsCollectionView.dequeueReusableCell(withReuseIdentifier: "CarouselViewCell",
                                                                                      for: indexPath) as? CarouselViewCell {
                featuredListCell.list = itemsUserCreatedLists[indexPath.row]
                cell = featuredListCell
            }
            return cell
        }
    }
}
//Apply edges and size of cell
extension ListsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        switch collectionView {
        case featuredListsCollectionView:
            return UIEdgeInsets(top: 0,
                                left: 4,
                                bottom: 0,
                                right: 4)
        case userCreatedListsCollectionView:
            return UIEdgeInsets(top: 0,
                                left: 8,
                                bottom: 0,
                                right: 8)
        default:
            return UIEdgeInsets(top: 0,
                                left: 10,
                                bottom: 0,
                                right: 10)
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case featuredListsCollectionView:
            return CGSize(width: 386,
                          height: 125)
        case userCreatedListsCollectionView:
            return CGSize (width: 160,
                           height: 140)
        default:
            return CGSize(width: 170,
                          height: 200)
        }
    }
}
//Scroll to center of section
extension ListsViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == favoritePlacesCollectionView {
            if let layout = self.favoritePlacesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                let minimum = layout.minimumLineSpacing
                let cellWidthIncludingSpacing = layout.itemSize.width + layout.sectionInset.left
                var offset = targetContentOffset.pointee
                let index = (offset.x + layout.sectionInset.left) / cellWidthIncludingSpacing
                let roundedIndex = round(index)
                currentPage = Int(roundedIndex)
                let xOffset = index * cellWidthIncludingSpacing - layout.sectionInset.left
                offset = CGPoint(x: (xOffset - minimum) + 1, y: 0)
                targetContentOffset.pointee = offset
                print("\(currentPage)")
            }
        }
    }
}
