//
//  FeaturedListsVC.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class FeaturedListsVC: UIViewController {

    @IBOutlet weak var featuredLists: UICollectionView!

    private var itemsFeaturedList: [FeaturedList] = {
        var firstCell = FeaturedList()
        firstCell.description = " l u n c h"
        firstCell.title = "Lunch"
        firstCell.mainImage = "lunchImage"

        return [firstCell, firstCell, firstCell, firstCell, firstCell, firstCell, firstCell, firstCell, firstCell]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Featured Lists"

        featuredLists.delegate = self
        featuredLists.dataSource = self
        let nibCell = UINib(nibName: "CarouselViewCell", bundle: nil)
        featuredLists.register(nibCell, forCellWithReuseIdentifier: "CarouselViewCell")
    }

}
extension FeaturedListsVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return itemsFeaturedList.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       var cell = UICollectionViewCell()
        if let featuredListscell = featuredLists.dequeueReusableCell(withReuseIdentifier: "CarouselViewCell",
                                                                     for: indexPath) as? CarouselViewCell {
             featuredListscell.list = itemsFeaturedList[indexPath.row]
            cell = featuredListscell
        }
        return cell
    }
}
extension FeaturedListsVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 386, height: 125)
    }
}
