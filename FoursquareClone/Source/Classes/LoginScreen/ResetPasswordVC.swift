//
//  ResetPasswordVC.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit
import Stripe

class ResetPasswordVC: UIViewController {
    // MARK: Properties
    @IBOutlet weak var descriptionsPasswordReset: UITextView!
    @IBOutlet weak var emailAddressField: UITextField?
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var stripePayment: UIButton!
    @IBOutlet weak var applePayButton: PKPaymentButton!
    @IBOutlet weak var shippingButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        stripePayment.addTarget(self, action: #selector(presentPaymentMethodsViewController), for: .touchUpInside)
        applePayButton.addTarget(self, action: #selector(applePay), for: .touchUpInside)
        shippingButton.addTarget(self, action: #selector(presentShippingViewController), for: .touchUpInside)
    }
    @objc private func presentShippingViewController() {
        let config = STPPaymentConfiguration()
        config.requiredShippingAddressFields = [.postalAddress]
        let viewController = STPShippingAddressViewController(configuration: config,
                                                              theme: .default(),
                                                              currency: "usd",
                                                              shippingAddress: nil,
                                                              selectedShippingMethod: nil,
                                                              prefilledInformation: nil)
        viewController.delegate = self
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
    @objc private func presentPaymentMethodsViewController() {
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        navigationController?.pushViewController(addCardViewController, animated: true)
    }
    @objc private func applePay() {
        let paymentNetworks: [PKPaymentNetwork] = [.amex, .masterCard, .visa]
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            let request = PKPaymentRequest()
            request.merchantIdentifier = "merchant.com.testnixsolutions.FoursquareCloneSelfStudy"
            request.countryCode = "CA"
            request.currencyCode = "CAD"
            request.supportedNetworks = paymentNetworks
            request.requiredShippingContactFields = [.name, .postalAddress]
            request.merchantCapabilities = .capability3DS
            let tshirt = PKPaymentSummaryItem(label: "T-shirt", amount: NSDecimalNumber(decimal: 1.00), type: .final)
            let shipping = PKPaymentSummaryItem(label: "Shipping", amount: NSDecimalNumber(decimal: 1.00), type: .final)
            let tax = PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(decimal: 1.00), type: .final)
            let total = PKPaymentSummaryItem(label: "Total", amount: NSDecimalNumber(decimal: 3.00), type: .final)
            request.paymentSummaryItems = [tshirt, shipping, tax, total]
            let authorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: request)
            if let viewController = authorizationViewController {
                viewController.delegate = self
                present(viewController, animated: true, completion: nil)
            }
        }
    }
}
extension ResetPasswordVC: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController,
                                            didAuthorizePayment payment: PKPayment,
                                            handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        STPAPIClient.shared().createToken(with: payment) { (stripeToken, error) in
            guard error == nil, let stripeToken = stripeToken else {
                print(error!)
                return
            }
            let url = URL(string: "http://localhost:3000/pay")
            guard let apiUrl = url else {
                print("Error creating url")
                return
            }
            var request = URLRequest(url: apiUrl)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            let body: [String: Any] = [ "stripeToken": stripeToken.tokenId,
                                        "amount": 300,
                                        "description": "Purchase of a t-shirt" ]
            try? request.httpBody = JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            let task = URLSession.shared.dataTask(with: request) { _, response, error in
                guard error == nil else {
                    print (error!)
                    completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
                    return
                }
                guard let response = response else {
                    print ("Empty or erronous response")
                    completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
                    return
                }
                print (response)
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                    completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
                } else {
                    completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
                }
            }
            task.resume()
        }
    }
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
}
extension ResetPasswordVC: STPAddCardViewControllerDelegate {
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true)
    }
    func addCardViewController(_ addCardViewController: STPAddCardViewController,
                               didCreateToken token: STPToken,
                               completion: @escaping STPErrorBlock) {
        StripeClient.shared.completeCharge(with: token, amount: 300 ) { result in
            switch result {
            case .success:
                completion(nil)
                let alertController = UIAlertController(title: "Congrats", message: "Your payment was successful!", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
extension ResetPasswordVC: STPShippingAddressViewControllerDelegate {
    func shippingAddressViewControllerDidCancel(_ addressViewController: STPShippingAddressViewController) {
        dismiss(animated: true, completion: nil)
    }
    func shippingAddressViewController(_ addressViewController: STPShippingAddressViewController,
                                       didFinishWith address: STPAddress,
                                       shippingMethod method: PKShippingMethod?) {
        StripeClient.shared.completeShipping(with: address, amount: 300 ) { result in
            switch result {
            case .success:
                let alertController = UIAlertController(title: "Congrats", message: "Your payment was successful!", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true)
            case .failure(let error):
            }
        }
    }
    func shippingAddressViewController(_ addressViewController: STPShippingAddressViewController,
                                       didEnter address: STPAddress,
                                       completion: @escaping STPShippingMethodsCompletionBlock) {
        let upsGround = PKShippingMethod()
        upsGround.amount = 0
        upsGround.label = "UPS Ground"
        upsGround.detail = "Arrives in 3-5 days"
        upsGround.identifier = "ups_ground"
        let upsWorldwide = PKShippingMethod()
        upsWorldwide.amount = 10.99
        upsWorldwide.label = "UPS Worldwide Express"
        upsWorldwide.detail = "Arrives in 1-3 days"
        upsWorldwide.identifier = "ups_worldwide"
        let fedEx = PKShippingMethod()
        fedEx.amount = 5.99
        fedEx.label = "FedEx"
        fedEx.detail = "Arrives tomorrow"
        fedEx.identifier = "fedex"
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            if address.country == nil || address.country == "US" {
                completion(.valid, nil, [upsGround, fedEx], fedEx)
            } else if address.country == "AQ" {
                let error = NSError(domain: "ShippingError", code: 123,
                                    userInfo: [NSLocalizedDescriptionKey: "Invalid Shipping Address",
                                               NSLocalizedFailureReasonErrorKey: "We can't ship to this country."])
                completion(.invalid, error, nil, nil)
            } else {
                fedEx.amount = 20.99
                completion(.valid, nil, [upsWorldwide, fedEx], fedEx)
            }
        }
    }
}
