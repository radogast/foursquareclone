//
//  LogInVC.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class LogInVC: UIViewController {
    // MARK: Properties
    @IBOutlet weak var emailField: UITextField?
    @IBOutlet weak var passwordField: UITextField?

    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var facebokSignInButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
    }
    private func addTargets() {
        forgotPasswordButton.addTarget(self, action: #selector(forgetPasswordButtonAction), for: .touchUpInside)
    }
    // MARK: Actions
    @objc private func forgetPasswordButtonAction() {
        ProfileRouterEmail.shared.goToPasswordReset(from: self)
    }

}
