//
//  AuthVC.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 12/28/18.
//  Copyright © 2018 Danil Verbytski. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FBSDKShareKit
import FBSDKLoginKit

class AuthVC: UIViewController {
    // MARK: Properties
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var signInWithFacebookButton: FBSDKLoginButton!
    @IBOutlet weak var createAccountDescription: UITextView!
    @IBOutlet weak var facebookAccountDescription: UITextView!
    @IBOutlet weak var shareFacebookButton: FBSDKSharingButton!
    @IBOutlet weak var shareWithActivityVCButton: UIButton!
    var videoReward: GADRewardBasedVideoAd!
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setupLayer()
        adSetup()
        facebokIntegration()
    }
    private func facebokIntegration() {
        let linkContent = FBSDKShareLinkContent()
        linkContent.contentURL = URL(string: "https://www.facebook.com")
        linkContent.quote = "Learn quick and simple ways for people to share content from your app or website to Facebook."
        linkContent.hashtag = FBSDKHashtag(string: "#MadeWithHackbook")
        shareFacebookButton.shareContent = linkContent
    }
    private func setupLayer() {
        createAccountDescription.textAlignment = .center
        facebookAccountDescription.textAlignment = .center
        signInWithFacebookButton.layer.cornerRadius = 2.0
    }
    private func adSetup() {
        videoReward = createVideoReward()
    }
    private func addTargets() {
        logInButton.addTarget(self, action: #selector(passwordReset), for: .touchUpInside)
        shareWithActivityVCButton.addTarget(self, action: #selector(shareActivityVC), for: .touchUpInside)
    }
   // MARK: Actions
    @objc private func shareActivityVC() {
        let activityVC = UIActivityViewController(activityItems: ["hello hello hello"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    @objc private func loginButtonAction() {
        ProfileRouterEmail.shared.goToLogin(from: self)
    }
    @objc private func signInButtonAction() {
        ProfileRouterEmail.shared.goToSingUp(from: self)
    }
    @objc private func passwordReset() {
        ProfileRouterEmail.shared.goToPasswordReset(from: self)
    }
}
extension AuthVC: GADRewardBasedVideoAdDelegate {
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward) {
        print(reward)
    }
    func createVideoReward() -> GADRewardBasedVideoAd {
        videoReward = GADRewardBasedVideoAd.sharedInstance()
        videoReward.delegate = self
        videoReward.load(GADRequest(),
                         withAdUnitID: "ca-app-pub-3940256099942544/1712485313")
        return videoReward
    }
    @objc private func showVideo() {
        if GADRewardBasedVideoAd.sharedInstance().isReady {
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
        }
    }
}
