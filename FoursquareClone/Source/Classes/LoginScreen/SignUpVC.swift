//
//  SignUpVC.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    let datePickereView: UIDatePicker = {
        let picker = UIDatePicker()
        picker.maximumDate = Date()
        return picker
    }()
}
