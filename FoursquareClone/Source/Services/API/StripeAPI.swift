//
//  StripeAPI.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 2/7/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
import Alamofire
import Stripe

enum StripeResult {
    case success
    case failure(Error)
}

final class StripeClient {
    static let shared = StripeClient()
    private init() {
        // private
    }
    private lazy var baseURL: URL = {
        guard let url = URL(string: "http://localhost:4567") else {
            fatalError("Invalid URL")
        }
        return url
    }()
    func completeCharge(with token: STPToken,
                        amount: Int,
                        completion: @escaping (StripeResult) -> Void) {
        let url = baseURL.appendingPathComponent("charge")
        let params: [String: Any] = [
            "token": token.tokenId,
            "amount": amount,
            "currency": "usd",
            "description": "Purchase from Foursquare iOS"
        ]
        Alamofire.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseString { response in
                switch response.result {
                case .success:
                    completion(StripeResult.success)
                case .failure(let error):
                    completion(StripeResult.failure(error))
                }
        }
    }
    func completeShipping(with address: STPAddress,
                          amount: Int,
                          completion: @escaping (StripeResult) -> Void ) {
        let url = baseURL.appendingPathComponent("Shippping")
        let params: [String: Any] = [
            "address": address.allResponseFields,
            "amount": amount,
            "currency": "usd",
            "description": "Purchase from Foursquare iOS"
            ]
        Alamofire.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseString { response in
                switch response.result {
                case .success:
                    completion(StripeResult.success)
                case .failure(let error):
                    completion(StripeResult.failure(error))
                }
        }
    }
}
