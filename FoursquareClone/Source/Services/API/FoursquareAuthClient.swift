//
//  FoursquareAuthClient.swift
//  FoursquareAPIClient
//
//  Created by Danil Verbytski on 1/2/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import UIKit

@objc public protocol FoursquareAuthClientDelegate {
    func foursquareAuthClientDidSucceed(accessToken: String)
    @objc optional func foursquareAuthClientDidFail(error: Error)
}

public class FoursquareAuthClient: NSObject {

    var clientId: String
    var callback: String
    weak var delegate: FoursquareAuthClientDelegate?

    public init(clientId: String, callback: String, delegate: FoursquareAuthClientDelegate) {
        self.clientId = clientId
        self.callback = callback
        self.delegate = delegate
    }

    public func authorizeWithRootViewController(_ controller: UIViewController) {
        let viewController = FoursquareAuthViewController(clientId: clientId, callback: callback)
        viewController.delegate = self
        let naviController = UINavigationController(rootViewController: viewController)
        controller.present(naviController, animated: true, completion: nil)
    }
}

// MARK: - FoursquareAuthViewControllerDelegate

extension FoursquareAuthClient: FoursquareAuthViewControllerDelegate {

    func foursquareAuthViewControllerDidSucceed(accessToken: String) {
        delegate?.foursquareAuthClientDidSucceed(accessToken: accessToken)
    }

    func foursquareAuthViewControllerDidFail(error: Error) {
        delegate?.foursquareAuthClientDidFail?(error: error)
    }
}
