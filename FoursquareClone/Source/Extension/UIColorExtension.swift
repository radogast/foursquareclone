//
//  UIColorExtension.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/22/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    convenience init(hexFromString: String, alpha: CGFloat = 1.0) {
        let cString: String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue: UInt32 = 10066329
        if (cString.count) == 6 {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
