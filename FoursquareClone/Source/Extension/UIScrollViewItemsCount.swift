//
//  UIScrollViewItemsCount.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/22/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
import UIKit
extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x+(0.5*self.frame.size.width))/self.frame.width)
    }
}
