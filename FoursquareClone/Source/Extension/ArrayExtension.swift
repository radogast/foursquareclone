//
//  ArrayExtension.swift
//  FoursquareClone
//
//  Created by Danil Verbytski on 1/25/19.
//  Copyright © 2019 Danil Verbytski. All rights reserved.
//

import Foundation
extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}
